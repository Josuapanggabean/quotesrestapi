<?php

require APPPATH . 'libraries/REST_Controller.php';

class Quotes extends REST_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->model('M_quotes');
  }

  public function index_get(){
      if($this->get('quote_id')!=null){
        $response = $this->M_quotes->quote_by_id($this->get('quote_id'));
        $this->response($response);
      }
      elseif($this->get('class_id')!=null){
        $response = $this->M_quotes->quote_by_class($this->get('class_id'));
        $this->response($response);
      }
      elseif($this->get('my_quote')!=null){
        $response = $this->M_quotes->quote_by_nim($this->get('my_quote'));
        $this->response($response);
    }
    elseif($this->get()==null){
        $response = $this->M_quotes->all_quotes();
        $this->response($response);
    }
    
    
  }

  public function index_post(){

    if($this->get('edit')!=null){
      $response = $this->M_quotes->update_quote($this->post(),$_FILES);
      $this->response($response);
    }
  elseif($this->get()==null){
    $response = $this->M_quotes->add_quote($this->post(),$_FILES);
    $this->response($response);
  }
  }

  public function index_put(){
    $response = $this->M_quotes->update_quote($this->post(),$_FILES);
    $this->response($response);
  }

  public function index_delete(){
    $response = $this->M_quotes->delete_quote(
        $this->get('quote_id')
      );
    $this->response($response);
  }

}

?>
